#ifndef PILGRIMJAM2020_GAMEMANAGER_H
#define PILGRIMJAM2020_GAMEMANAGER_H

#include <SFML/Graphics.hpp>

class GameManager {
    sf::RenderWindow window;

public:
    void create(sf::Vector2i size, std::string title);
    void run();
};

#endif //PILGRIMJAM2020_GAMEMANAGER_H
