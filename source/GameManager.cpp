#include "GameManager.h"

void GameManager::create(sf::Vector2i size, std::string title) {
    window.create(sf::VideoMode(size.x, size.y, 32), title);
}
void GameManager::run() {
    sf::CircleShape shape(100.f);
    shape.setFillColor(sf::Color::Green);

    while (window.isOpen())
    {
        sf::Event event;
        while (window.pollEvent(event))
        {
            if (event.type == sf::Event::Closed)
                window.close();
        }

        window.clear();
        window.draw(shape);
        window.display();
    }
}