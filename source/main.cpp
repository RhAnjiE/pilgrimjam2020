#include "GameManager.h"

int main()
{
    GameManager gameManager;

    gameManager.create(sf::Vector2i(1366, 768), "Pilgrim Jam 2020");
    gameManager.run();

    return 0;
}